/**
 * 深拷贝函数
 * @param { * } value 需要深拷贝的值
 * @returns
 */
function cloneDeep(value) {
    if (value === null) return null;

    if (value instanceof Set) return new Set([...value]);

    if (value instanceof Map) return new Map([...value]);

    if (value instanceof WeakMap) {
        let weakMap = new WeakMap();
        weakMap = value;
        return weakMap;
    };
    
    if (value instanceof WeakSet) {
        let weakSet = new WeakSet();
        weakSet = value;
        return weakSet;
    };

    if (value instanceof RegExp) return new RegExp(value);

    if (typeof value === "undefined") return undefined;

    if (Array.isArray(value)) return value.map(cloneDeep);

    if (value instanceof Date) return new Date(value.getTime());

    if (typeof value !== "object") return value;

    const _value = {};
    for (const [key, value] of Object.entries(value)) {
        _value[key] = cloneDeep(value);
    };

    const symbolkeys = Object.getOwnPropertySymbols(value);
    for (const key of symbolkeys) {
        _value[key] = cloneDeep(value[key]);
    };

    return _value;
};

export default cloneDeep;
