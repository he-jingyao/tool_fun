## 数据深拷贝

> 数据深拷贝, 提取自 lodash, 最小化模块体积

Demo:

```js | pure
import { cloneDeep } from 'yyyao';

const _value = cloneDeep([1, 2, 3, 4, 5]);
console.log(_value); // -> [1, 2, 3, 4, 5]
```
